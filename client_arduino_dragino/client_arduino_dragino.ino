#include <SPI.h>
#include <RH_RF95.h>
#include <ArduinoJson.h>
RH_RF95 rf95;
String ID = "123abc";

int counter = 0;
StaticJsonBuffer<200> jsonBuffer;
void setup()

{
  Serial.begin(9600);
  while (!Serial) ; // Wait for serial port to be available
  if (!rf95.init())
    Serial.println("init failed");
}
void loop()
{


  Serial.println("Sending to rf95_server");
  String d = "Data";
  String x = "{\"ID\":\"" + ID + "\",\"TYPE\":\""+d+"\",\"DATA\":\"hello\"}";
  int length_arr = x.length();
  length_arr++;
  char data[length_arr];
  x.toCharArray(data, length_arr);
  //uint8_t data[100] = " \"ID\":\"AAAAA\",\"Message\": \"Hello World!\", \"Data\":[1,2], \"info\": \"abc\"";
  rf95.send(data, sizeof(data));
  rf95.waitPacketSent();  // Send a message to rf95_server
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN]; // Now wait for a reply
  uint8_t len = sizeof(buf);
  counter++;
  if (rf95.waitAvailableTimeout(3000))
  {
    // Should be a reply message for us now
    if (rf95.recv(buf, &len))
    {
      //Serial.print("got reply: ");
      //Serial.println((char*)buf);
      String str = ((char*)buf);
      Serial.println(str);
      //str = "{" + str + "}";
      //Serial.println(str);
      int length_arr = str.length();
      length_arr++;
      char json[length_arr];
      str.toCharArray(json, length_arr);
      JsonObject& root = jsonBuffer.parseObject(json);
      if (!root.success()) {
        Serial.println("parseObject() failed");
        return;
      }
      if (root["ID"] == "SERVER") {
        if (root["TYPE"] == "OK")
        {
          Serial.println("MESSAGE SEND CORRECTLY");
        }
        else if (root["TYPE"] == "KEY")
        {
          Serial.println("STARTING D.F.");
          int key = df_key();
          Serial.println(key);
        }
      }
      else{
        Serial.print("Uncnown");
        }
    }
    else
    {
      Serial.println("recv failed");
    }
  }
  else
  {
    Serial.println("No reply, is rf95_server running?");
  }
  jsonBuffer.clear();
  delay(400);
}
int df_key() {
  uint8_t buf1[RH_RF95_MAX_MESSAGE_LEN];
  uint8_t len1 = sizeof(buf1);
  int  p_m = 41;
  int  g = 8;
  int alice = 53;
  int to_send = (g ^ alice) % p_m;
  String x = "{\"TYPE\":\"KEY\",\"DATA\":\"" + String(to_send) + "\"}";
  //String x ="78";
  int length_arr = x.length();
  length_arr++;
  char data[length_arr];
  x.toCharArray(data, length_arr);
  // char data[4]={"1","2","3"};
  Serial.println("STELNO : ");
  Serial.println(data);
  rf95.send(data, sizeof(data));
  rf95.waitPacketSent();
  jsonBuffer.clear();
  return to_send;
}



